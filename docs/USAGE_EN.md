# Webservice ph5-dataselect

## Description

The ph5-dataselect service provides access to PH5 seismic data of the RESIF network.

## Output format options

  - miniSEED
  - SAC binary
  - GEOCSV (TSPAIR)

## Query usage

    /query? (channel-options) (date-range-options) [output-options] [filter-options] [nodata=404]

    where:

    channel-options      ::  (net=<network>) (sta=<station>) (loc=<location>) (cha=<channel>)
    date-range-options   ::  (starttime=<date|duration>) (endtime=<date|duration>)
    output-options       ::  [format=<geocsv|mseed|sac>]
    filter-options       ::  [decimate=<2-16>] [reduction=<number>]

    (..) required
    [..] optional

## Sample queries

<a href="http://ph5ws.resif.fr/fdsnws/dataselect/1/query?net=3C&station=N02&channel=DPN&start=2019-11-13T12:00:00&end=2019-11-13T12:50:00&nodata=404&format=sac">http://ph5ws.resif.fr/fdsnws/dataselect/1/query?net=3C&station=N02&channel=DPN&start=2019-11-13T12:00:00&end=2019-11-13T12:50:00&nodata=404&format=sac</a>

## Detailed descriptions of each query parameter

### Channel options

| Parameter  | Example  | Discussion                                                                       |
| :--------- | :------- | :--------------------------------------------------------------------------------|
| net[work]  | FR       | Seismic network name. Accepts wildcards and lists.                               |
| sta[tion]  | CIEL     | Station name. Accepts wildcards and lists.                                       |
| loc[ation] | 00       | Location code. Use loc=-- for empty location codes. Accepts wildcards and lists. |
| cha[nnel]  | HHZ      | Channel Code. Accepts wildcards and lists.                                       |

#### Wildcards and lists

  - Wildcards: the question mark __?__ represents any single character, while the asterisk __*__ represents zero or more characters.

  - List: multiple items may be retrieved using a comma-separated list. Wildcards may be included in the list.

For example, with channel codes: channel=EH?,BHZ

### Date-range options

| Parameter   | Example             | Discussion                                         |
| :---------- | :------------------ | :------------------------------------------------- |
| start[time] | 2010-01-10T00:00:00 | Selects data on or after the specified start time. |
| end[time]   | 2011-02-11T01:00:00 | Selects data on or before the specified end time.  |

The definition of the time interval may take different forms:

  - a calendar dates, for example starttime=2015-08-12T01:00:00
  - duration in seconds, for example endtime=7200
  - the key word "currentutcday" which means midnight of today’s date (UTC time), for example starttime=currentutcday

### Time series processing options

| Parameters | Examples | Discussion                                                       | Default value |
| :--------- | :------- | :--------------------------------------------------------------- | :------------ |
| reduction  | 1.5      | Reduce velocity by the specified factor.                         | none          |
| deci[mate] | 2.0      | Decimation factor.                                               | none          |

### Output options
| Parameters | Example |                                 Discussion                                    | Default value |
| :--------- | :------ | :---------------------------------------------------------------------------- | :------------ |
| format     | sac     | Format of the output file : geocsv, mseed, sac                                | mseed  |
| nodata     | 404     | Specify which HTTP status code is returned when no data is found (204 or 404) | 204    |

## HTTP POST queries

The general form of a POST is parameter=value pairs, one per line, followed by an arbitrary number of channels and, optionally, time window selection lines:

parameter=\<value\> \
parameter=\<value\> \
parameter=\<value\> \
Net Sta Loc Chan [StartTime EndTime] \
Net Sta Loc Chan [StartTime EndTime] \
...

Start time and end times can be specified globally, such as

... \
start=2020-10-01T00:00:00 \
end=2020-10-01T00:01:00\
Net1 Sta1 Loc1 Chan1 \
Net2 Sta2 Loc2 Chan2 \
...

or per line:

... \
Net1 Sta1 Loc1 Chan1 2020-10-01T00:00:00 2020-10-01T00:01:00 \
Net2 Sta2 Loc2 Chan2 2020-10-02T00:00:00 2020-10-02T00:02:00 \
...

If not given, the start and end times default to the fully available time range. Additionally, global time ranges can be mixed with individual time ranges.

## Date and time formats

    YYYY-MM-DDThh:mm:ss[.ssssss] ex. 1997-01-31T12:04:32.123
    YYYY-MM-DD ex. 1997-01-31 (a time of 00:00:00 is assumed)

    where:

    YYYY    :: four-digit year
    MM      :: two-digit month (01=January, etc.)
    DD      :: two-digit day (01 through 31)
    T       :: date-time separator
    hh      :: two-digit hour (00 through 23)
    mm      :: two-digit number of minutes (00 through 59)
    ss      :: two-digit number of seconds (00 through 59)
    ssssss  :: one to six-digit number of microseconds (0 through 999999)

