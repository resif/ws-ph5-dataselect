# Building the PH5 suite
FROM python:2.7-slim-buster as ph5-build
RUN apt-get update && apt-get install -y git
RUN git clone https://github.com/PIC-IRIS/PH5.git
WORKDIR PH5
RUN pip install numpy
RUN apt-get install -y build-essential
RUN pip install -e .

# Building heavy python dependencies as a first stage
# will build ospy for instance
FROM python:3.9-slim-buster AS python-deps
RUN apt-get update && apt-get install -y --no-install-recommends gcc libc6-dev
COPY requirements.txt /
RUN pip install --no-cache-dir -r /requirements.txt


FROM python:3.9-slim-buster
# On copie les sources de PH5, et tous les packages qui ont été compilés précédemment dans le container python2.7
# On installe python2.7 pour que PH5 fonctionne
RUN apt-get update && apt-get install -y python-pip
COPY --from=ph5-build /PH5 /PH5
COPY --from=ph5-build /usr/local/lib/python2.7/site-packages /usr/lib/python2.7/dist-packages
RUN pip2 install -e /PH5
# Get the dependencies from previous stage
COPY --from=python-deps /usr/local/lib/python3.9/site-packages /usr/local/lib/python3.9/site-packages
RUN pip install --no-cache-dir gunicorn
WORKDIR /appli
COPY start.py config.py ./
COPY apps ./apps/
COPY templates ./templates/
COPY static ./static/
USER 1000

CMD ["/bin/bash", "-c", "gunicorn --bind 0.0.0.0:8000 start:app"]
