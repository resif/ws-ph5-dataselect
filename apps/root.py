import io
import os
import logging
import queue
import re
from multiprocessing import Process, Queue

from flask import request

from apps.globals import DECI_MIN, DECI_MAX
from apps.globals import Error
from apps.globals import HTTP
from apps.globals import MAX_DAYS
from apps.globals import TIMEOUT
from apps.output import get_output, too_much_data
from apps.parameters import Parameters
from apps.utils import check_base_parameters
from apps.utils import check_request
from apps.utils import error_param
from apps.utils import error_request
from apps.utils import is_valid_integer
from apps.utils import is_valid_nodata
from apps.utils import is_valid_output


def check_parameters(params):

    # check base parameters
    (params, error) = check_base_parameters(params, MAX_DAYS)
    if error["code"] != 200:
        return (params, error)

    # Decimation parameter validation
    if params["deci"] is not None:
        if not is_valid_integer(params["deci"], DECI_MIN, DECI_MAX):
            return error_param(params, Error.DECI + str(params["deci"]))
        params["deci"] = int(params["deci"])

    # output parameter validation
    if not is_valid_output(params["format"]):
        return error_param(params, Error.OUTPUT + str(params["format"]))
    params["format"] = params["format"].lower()

    # nodata parameter validation
    if not is_valid_nodata(params["nodata"]):
        return error_param(params, Error.NODATA_CODE + str(params["nodata"]))
    params["nodata"] = params["nodata"].lower()

    for key, val in params.items():
        logging.debug(key + ": " + str(val))

    return (params, {"msg": HTTP._200_, "details": Error.VALID_PARAM, "code": 200})


def checks_get():

    # get default parameters
    params = Parameters().todict()

    # check if the parameters are unknown
    (p, result) = check_request(params)
    if result["code"] != 200:
        return (p, result)
    return check_parameters(params)


def checks_post(params):

    for key, val in Parameters().todict().items():
        if key not in params:
            params[key] = val
    return check_parameters(params)


def get_post_params():
    rows = list()
    params = dict()
    paramslist = list()
    code = ("network", "station", "location", "channel")
    exclude = ("net", "sta", "cha", "loc", "starttime", "endtime", "constraints")
    post_params = (k for k in Parameters().todict() if k not in exclude)

    # Universal newline decoding :
    stream = io.StringIO(request.stream.read().decode("UTF8"), newline=None)
    for row in stream:
        row = row.strip()  # Remove leading and trailing whitespace.
        row = re.sub(r"[\s]+", " ", row)
        if re.search(r"[^a-zA-Z0-9_,* =?.:-]", row) is None:  # Is a valid row ?
            if re.search(r"=", row):  # parameter=value pairs
                key, val = row.replace(" ", "").split("=")
                if key in post_params:
                    params[key] = val
            else:
                rows.append(row)
    for row in rows:
        row = row.split(" ")
        if len(row) >= 4:
            paramslist.append({code[i]: row[i] for i in range(0, 4)})
            paramslist[-1].update(params)
            if len(row) == 6:  # Per line start time and end time.
                paramslist[-1].update({"start": row[4], "end": row[5]})
    return paramslist


def output(authuser=None):
    process = None
    try:
        valid_param_dicts = list()
        result = {"msg": HTTP._400_, "details": Error.UNKNOWN_PARAM, "code": 400}
        logging.debug(request.url)

        if request.method == "POST":
            for params in get_post_params():
                (params, result) = checks_post(params)
                if result["code"] == 200:
                    params["authuser"] = authuser
                    valid_param_dicts.append(params)
        else:
            (params, result) = checks_get()
            if result["code"] == 200:
                params["authuser"] = authuser
                valid_param_dicts.append(params)

        logging.info(f"New request with parameters: {valid_param_dicts}")
        data_limit = int(os.getenv("DATALIMIT", "320"))
        if too_much_data(valid_param_dicts, data_limit):
            # Si c'est trop, on renvoie un code 413 avec un message sympa quand même
            logging.info("User is asking for too much data. Return 413 (kindly)")
            result = {
                "msg": HTTP._413_,
                "details": f"The result would be larger than {data_limit} samples of cumulated data. You should split the request in smaller parts. If you want an entire dataset, look for https://seismology.resif.fr/rsync/",
                "code": 413,
            }
        elif valid_param_dicts:

            def put_response(q):
                q.put(get_output(valid_param_dicts))

            q = Queue()
            process = Process(target=put_response, args=(q,))
            process.start()
            resp = q.get(timeout=TIMEOUT)

            if resp:
                return resp
            else:
                raise Exception

    except queue.Empty:
        result = {"msg": HTTP._408_, "details": Error.TIMEOUT, "code": 408}

    except Exception as excep:
        result = {"msg": HTTP._500_, "details": Error.UNSPECIFIED, "code": 500}
        logging.exception(str(excep))

    finally:
        if process:
            process.terminate()

    return error_request(
        msg=result["msg"], details=result["details"], code=result["code"]
    )
