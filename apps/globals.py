# limitations
TIMEOUT = None
MAX_DAYS = None

# available parameter values
OUTPUT = ("geocsv", "mseed", "sac")
NODATA_CODE = ("204", "404")
STRING_TRUE = ("yes", "true", "t", "y", "1", "")
STRING_FALSE = ("no", "false", "f", "n", "0")

# processing constants
DECI_MIN = 1
DECI_MAX = 16

# error message constants
DOCUMENTATION_URI = "http://ph5ws.resif.fr/fdsnws/dataselect/1/"
SERVICE = "ph5ws-dataselect"
VERSION = "1.0.0"


class Error:
    UNKNOWN_PARAM = "Unknown query parameter: "
    MULTI_PARAM = "Multiple entries for query parameter: "
    VALID_PARAM = "Valid parameters."
    START_LATER = "The starttime cannot be later than the endtime: "
    TOO_LONG_DURATION = "Too many days requested (greater than "
    RESPONSE = "Deconvolution can't be performed. Instrumental response not available."
    UNSPECIFIED = "Error processing your request."
    NODATA = "Your query doesn't match any available data."
    TIMEOUT = f"Your query exceeds timeout ({TIMEOUT} seconds)."
    MISSING = "Missing parameter: "
    BAD_VAL = " Invalid value: "
    CHAR = "White space(s) or invalid string. Invalid value for: "
    EMPTY = "Empty string. Invalid value for: "
    BOOL = "(Valid boolean values are: true/false, yes/no, t/f or 1/0)"
    NETWORK = "Invalid network code: "
    STATION = "Invalid station code: "
    LOCATION = "Invalid location code: "
    CHANNEL = "Invalid channel code: "
    TIME = "Bad date value: "
    OUTPUT = f"Accepted output values are: {OUTPUT}." + BAD_VAL
    INT_BETWEEN = "must be an integer between"
    DECI = f"decimate {INT_BETWEEN} {DECI_MIN} and {DECI_MAX}." + BAD_VAL
    PROCESSING = "Your request cannot be processed. Check for value consistency."
    NODATA_CODE = f"Accepted nodata values are: {NODATA_CODE}." + BAD_VAL
    NO_SELECTION = "Request contains no selections."


class HTTP:
    _200_ = "Successful request. "
    _204_ = "No data matches the selection. "
    _400_ = "Bad request due to improper value. "
    _401_ = "Authentication is required. "
    _403_ = "Forbidden access. "
    _404_ = "No data matches the selection. "
    _408_ = "Request exceeds timeout. "
    _413_ = "Request too large. "
    _414_ = "Request URI too large. "
    _500_ = "Internal server error. "
    _503_ = "Service unavailable. "
