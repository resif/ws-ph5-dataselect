class Parameters:
    def __init__(self):
        self.network = None
        self.station = None
        self.location = None
        self.channel = None
        self.starttime = None
        self.endtime = None
        self.net = "*"
        self.sta = "*"
        self.loc = "*"
        self.cha = "*"
        self.start = None
        self.end = None
        self.decimate = None
        self.deci = None
        self.reduction = None
        self.format = "mseed"
        self.nodata = "204"
        self.constraints = {
            "alias": [
                ("network", "net"),
                ("station", "sta"),
                ("location", "loc"),
                ("channel", "cha"),
                ("starttime", "start"),
                ("endtime", "end"),
                ("decimate", "deci"),
            ],
            "booleans": [],
            "floats": ["reduction"],
            "not_none": ["start", "end"],
        }

    def todict(self):
        return self.__dict__
