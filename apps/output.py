import json
import logging
import os
import re
import shutil
import time
import random
import string

from datetime import datetime
from subprocess import run
from tempfile import mkdtemp, NamedTemporaryFile
from zipfile import ZipFile, ZIP_DEFLATED

from flask import current_app, make_response, request
from obspy.core import read, Stream
import psycopg2
import redis

from apps.globals import Error
from apps.utils import error_request
from apps.utils import tictac


def requestid_generator():
    # Generates a random 8 characters string
    return "".join(
        random.choice(string.ascii_uppercase + string.digits) for x in range(8)
    )


def is_like_or_equal(params, key):
    """Builds the condition for the specified key in the "where" clause taking into account lists or wildcards."""

    subquery = []
    for param in params[key].split(","):
        op = "LIKE" if re.search(r"[*?]", param) else "="
        subquery.append(f"rph5.{key} {op} '{param}'")
    return " OR ".join(subquery)


def sql_request(params):
    """Builds the PostgreSQL request."""

    s = "SELECT rph5.network, station, location, channel, starttime, endtime, iarray, source_file, samplerate::numeric FROM"
    if params["authuser"] is not None:
        s  = f"""{s} aut_user,"""
    s = f"""{s} rph5 WHERE ({is_like_or_equal(params, "network")})"""
    if params["station"] != "*":
        s = f"""{s} AND ({is_like_or_equal(params, "station")})"""
    if params["location"] != "*":
        s = f"""{s} AND ({is_like_or_equal(params, "location")})"""
    if params["channel"] != "*":
        s = f"""{s} AND ({is_like_or_equal(params, "channel")})"""

    start = "-infinity" if params["start"] is None else params["start"]
    end = "infinity" if params["end"] is None else params["end"]
    s = f"""{s} AND (starttime, endtime) OVERLAPS ('{start}', '{end}')"""

    if params["authuser"] is None:
        s = f"""{s} AND policy = 'open'"""
    else:
        s = f"""{s} AND (policy = 'open' OR (name = '{params["authuser"]}' AND aut_user.network = rph5.network AND EXTRACT(year FROM starttime) BETWEEN start_year AND end_year AND EXTRACT(year FROM endtime) BETWEEN start_year AND end_year))"""

    select = s.replace("?", "_").replace("*", "%").replace("--", "")
    return f"{select} ORDER BY network, station, location, channel, starttime, endtime;"


def get_statistics(temp_dir):
    """Returns metadata from rph5 table."""

    redisClient = redis.Redis(
        host=current_app.config["REDIS_HOST"],
        port=current_app.config["REDIS_PORT"],
        db=0,
    )

    st = read(os.path.join(temp_dir, "*.ms"))
    data = {
        "timestamp": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S") + "Z",
        "ws": "ph5ws-dataselect",
        "clientip": request.access_route[0],
        "requestid": requestid_generator(),
        "useragent": request.user_agent.string,
        "nbfiles": 1,
        "sizes": [],
    }
    for tr in st:
        data["sizes"].append(
            {
                "network": tr.stats.network,
                "station": tr.stats.station,
                "location": tr.stats.location,
                "channel": tr.stats.channel,
                "size": tr.stats.mseed.filesize,
            }
        )
    logging.debug("Send to redis: %s", data)
    try:
        redisClient.rpush(current_app.config["REDIS_QUEUE"], json.dumps(data))
    except Error as e:
        logging.warning("Error sending message:", e)


def ph5_metadata_from_database(params):
    """Returns metadata from rph5 table."""

    with psycopg2.connect(current_app.config["DATABASE_URI"]) as conn:
        logging.debug(conn.get_dsn_parameters())
        logging.debug("Postgres version : %s", conn.server_version)
        with conn.cursor() as curs:
            select = sql_request(params)
            logging.debug(f"SQL request to issue: {select}")
            curs.execute(select)
            logging.debug(curs.statusmessage)
            return curs.fetchall()


def get_file_type(params):
    if params["format"] == "sac":  # little-endian SAC
        file_type, file_ext = "SAC", ".sac"
    elif params["format"] == "geocsv":
        file_type, file_ext = "MSEED", ".csv"
    else:
        file_type, file_ext = "MSEED", ".mseed"
    return (file_type, file_ext)


def too_much_data(params_list, limit=0):
    """
    From the parameters, evaluate the quantity of data.
    - get the starttime and endtime of each concerned station
    - intersect with starttime and endtime from the parameters and get seconds
    - multiply by the sampling rate
    - if larger than limit, return True
    - else return False
    """
    estimated_samples = 0
    logging.debug("Checking for too much data")
    for params in params_list:
        logging.debug(sql_request(params))
        try:
            with psycopg2.connect(current_app.config["DATABASE_URI"]) as conn:
                with conn.cursor() as curs:
                    curs.execute(sql_request(params))
                    for row in curs.fetchall():
                        # Make the interval intersection
                        intersect = min(params["end"], row[5]) - max(params["start"], row[4])
                        if intersect.total_seconds() < 0:
                            estimated_samples = 0
                        else:
                            try:
                                estimated_samples += intersect.total_seconds() * int(row[8])
                            except TypeError as err:
                                logging.warning("Sampling rate %s for network %s has incorrect type. %s", row[8], row[0], err)
                        if estimated_samples > limit:
                            logging.info(f"{estimated_samples} estimated samples: Too much data. Stop here.")
                            return True
        except psycopg2.Error as err:
            logging.error("Database related problem: %s", err)
    logging.info("Data retrieval estimated at %s samples", estimated_samples)
    return False


def get_output(param_dic_list):
    """Builds the ws-ph5-dataselect response.
    param_dic_list: list of parameter dictionaries"""

    logging.debug(f"Parameters: {param_dic_list}")
    response = None
    try:
        tic = time.time()
        TEMP_DIR = mkdtemp()
        ARCHIVE_DIR = current_app.config["ARCHIVE_DIR"]
        PH5_DIR = current_app.config["PH5_DIR"]
        files = []

        # Extract common parameters
        common_params = param_dic_list[0]
        (file_type, file_ext) = get_file_type(common_params)
        basename = "resifws-ph5"
        fname = basename + file_ext

        # Iterate on all parameters
        for params in param_dic_list:
            locations = set()
            # Save locations from request
            for loc in params['location'].replace("--", "").split(","):
                locations.add(loc)

            # Run SQL query
            metadata = ph5_metadata_from_database(params)
            logging.debug(f"Metadata: {metadata}")
            if metadata is None:
                return None
            if not metadata:
                continue


            # Aggregate all results
            ph5_args = [set(i) for i in zip(*metadata)]
            logging.debug(f"PH5 args : {ph5_args}")

            # Build command line
            net = "--network " + ",".join(i for i in ph5_args[0])   # rph5.network
            sta = "--station " + ",".join(i for i in ph5_args[1])   # rph5.station
            cha = "--channel " + ",".join(i for i in ph5_args[3])   # rph5.channel
            array = "--array " + ",".join(i for i in ph5_args[6])   # rph5.iarray
            start = "--starttime " + params["start"].strftime("%Y-%m-%dT%H:%M:%S.%f")
            end = "--stoptime " + params["end"].strftime("%Y-%m-%dT%H:%M:%S.%f")

            cmd = f"{PH5_DIR}/ph5toms {net} {sta} {cha} {array} {start} {end}"
            if params["deci"]:
                cmd = cmd + " --decimation " + str(params["deci"])
            if params["reduction"]:
                cmd = cmd + " --reduction " + str(params["reduction"])

            # Run the command line on all source files
            for master in ph5_args[7]:                              # rph5.source_file
                master = f"-n {ARCHIVE_DIR}/{master}"
                cmd = f"{cmd} {master} -o {TEMP_DIR} -F {file_type}"
                logging.info(cmd)
                run(cmd, shell=True, check=False, executable="/bin/bash")

            # Iterate on all converted files to filter by location
            logging.info(f"Filtering on locations. Request specified: {locations}")
            if not locations or "*" in locations:
                logging.debug(f"keep: all files")
                files += list(os.scandir(TEMP_DIR))
            else:
                for entry in os.scandir(TEMP_DIR):
                    if entry.name.split(".")[2] in locations:
                        logging.debug(f"keep: {entry.path}")
                        files.append(entry)
                    else:
                        logging.debug(f"remove: {entry.path}")
                        os.remove(entry.path)

        # Return error if no files
        if not files:
            code = int(common_params["nodata"])
            return error_request(msg=f"HTTP._{code}_", details=Error.NODATA, code=code)

        # Build response
        if common_params["format"] == "mseed":
            get_statistics(TEMP_DIR)

            cmd = f"cd {TEMP_DIR} && cat *.ms > {fname}"
            run(cmd, shell=True, check=True, executable="/bin/bash")
            with open(TEMP_DIR + "/" + fname, mode="rb") as fid:
                data = fid.read()
            headers = {"Content-Disposition": f"attachment; filename={fname}"}
            response = make_response(data, headers)
            response.mimetype = "application/vnd.fdsn.mseed"

        elif common_params["format"] in ("geocsv", "sac"):

            if common_params["format"] == "geocsv":
                write_format = "TSPAIR"
                write_extension = ".csv"
            elif common_params["format"] == "sac":
                write_format = "SAC"
                write_extension = ".sac"

            tmp_zip = NamedTemporaryFile()
            with ZipFile(tmp_zip.name, "w", ZIP_DEFLATED) as fzip:
                for entry in files:
                    if entry.name.endswith(".ms"):
                        logging.debug(entry.path)
                        st = Stream()
                        st = st + read(entry.path)
                        st.write(entry.path.replace(".ms", write_extension), format=write_format)
                        fzip.write(entry.path.replace(".ms", write_extension), entry.name)

            headers = {"Content-Disposition": f"attachment; filename={basename}.zip"}
            response = make_response(tmp_zip.read(), headers)
            response.headers["Content-type"] = "application/x-zip-compressed"
            response.mimetype = "application/x-zip-compressed"

        # Return response
        return response
    except Exception as ex:
        logging.exception(str(ex))
    finally:
        if os.path.exists(TEMP_DIR):
            shutil.rmtree(TEMP_DIR)
        if response:
            nbytes = response.headers.get("Content-Length")
            logging.info(f"{nbytes} bytes rendered in {tictac(tic)} seconds.")
