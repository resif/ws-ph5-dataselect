import logging
import os

from flask import Flask, make_response, render_template
from flask_httpauth import HTTPDigestAuth
import psycopg2

from apps.globals import VERSION
from apps.root import output
from config import Config


app = Flask(__name__)

FMT = "[%(asctime)s] %(levelname)s [%(filename)s:%(lineno)d] [%(funcName)s] %(message)s"
loglevels = {"INFO": logging.INFO, "DEBUG": logging.DEBUG}
LOGLEVEL = loglevels.get(os.environ.get("LOGLEVEL", 'INFO'), logging.INFO)
logging.basicConfig(format=FMT, level=LOGLEVEL)

app.config.from_object(Config)
if app.config["RUNMODE"]:
    app.logger.debug("Configuration set with RUNMODE=%s", app.config["RUNMODE"])
app.logger.debug("Database URI: %s", app.config["DATABASE_URI"])
app.logger.debug("Archive DIR: %s", app.config["ARCHIVE_DIR"])
app.logger.debug("PH5 DIR: %s", app.config["PH5_DIR"])

auth = HTTPDigestAuth(realm=app.config["DIGEST_REALM"], use_ha1_pw=True)


def get_users(username):
    with psycopg2.connect(app.config["DATABASE_URI"]) as conn:
        logging.debug(conn.get_dsn_parameters())
        with conn.cursor() as curs:
            select = f"SELECT user_name, user_pass FROM ws_users WHERE user_name = '{username}'"
            curs.execute(select)
            return dict(curs.fetchall())


# ************************************************************************
# **************************** SERVICE ROUTES ****************************
# ************************************************************************


@auth.get_password
def get_pw(username):
    users = get_users(username)
    logging.debug(f"realm: {auth.realm}, username: {auth.username()}")
    if users and username in users:
        return users.get(username)
    logging.debug(f"Unknown user: {username}")
    return None


@app.route("/queryauth", methods=["GET", "POST"])
@auth.login_required
def dataselect_root_auth_get():
    return output(auth.username())


@app.route("/query", methods=["GET", "POST"])
def query():
    return output()


@app.route("/application.wadl")
def wadl():
    template = render_template("wadl.xml")
    response = make_response(template)
    response.headers["Content-Type"] = "application/xml"
    return response


@app.route("/version", strict_slashes=False)
def version():
    response = make_response(VERSION)
    response.headers["Content-Type"] = "text/plain"
    return response


@app.route("/commit", strict_slashes=False)
def commit():
    try:
        with open("./static/commit.txt") as commit_file:
            COMMIT_SHORT_SHA = commit_file.readline()
    except Exception:
        COMMIT_SHORT_SHA = "unspecified"
    response = make_response(COMMIT_SHORT_SHA)
    response.headers["Content-Type"] = "text/plain"
    return response


@app.route("/")
@app.route("/local=fr")
def doc():
    return render_template("doc.html")


@app.route("/local=en")
def doc_en():
    return render_template("doc_en.html")


# **** MAIN ****
if __name__ == "__main__":
    app.run()
